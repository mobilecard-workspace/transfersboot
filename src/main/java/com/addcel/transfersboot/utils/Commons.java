package com.addcel.transfersboot.utils;

import java.io.IOException;
import java.io.Reader;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import feign.Response;

@Component
public class Commons {

	private final static Logger logger = Logger.getLogger(Commons.class);
	
	public String parseFeignError(Response response) {
		String body = "";
		try {
			if (response.body() != null) {
				Reader reader = response.body().asReader();
				body = IOUtils.toString(reader);	
			} else {
				body = response.reason();
			}
			
		} catch (IOException e) {
			logger.error("Metodo parseFeignError: ", e);
		}
		return body; 
	}

}
