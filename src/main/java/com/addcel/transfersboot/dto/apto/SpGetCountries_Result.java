package com.addcel.transfersboot.dto.apto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SpGetCountries_Result implements Serializable {
	
	private static final long serialVersionUID = -4233160432386540962L;
	
	private String idCountry;
    private String nameCountry;

}
