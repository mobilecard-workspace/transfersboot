package com.addcel.transfersboot.dto.apto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CurrencyByCountry implements Serializable {

	private static final long serialVersionUID = -5390630561457548972L;

	private SpGetCurrenciesbyCountry_Result[] currency;
	
	private int idError;
	private String mensajeError;

}
