package com.addcel.transfersboot.dto.apto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SpGetReceiverPaymentLocations_Result implements Serializable {

	private static final long serialVersionUID = -4954058860519019176L;

	private String addressPaymentLocation;
	private String businessHours;
	private String idPaymentLocation;
	private String latitude;
	private String longitude;
	private String namePayer;
	private String namePaymentLocation;

}
