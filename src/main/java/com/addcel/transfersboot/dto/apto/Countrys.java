package com.addcel.transfersboot.dto.apto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Countrys implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String idCountry;
	private String idState;
	private String idCurrency;
	private String zipCode;
	private String cityName;
	private String currencyMode;
	private String payMode;
	private String idCity;
	private String operation;

}
