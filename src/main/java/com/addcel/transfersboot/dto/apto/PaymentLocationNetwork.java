package com.addcel.transfersboot.dto.apto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PaymentLocationNetwork implements Serializable {

	private static final long serialVersionUID = -8533812056964132977L;

	private String idCountry;
	private String idModePayCurrency;
	private String idPaymentMode;
	private double amount;
	private String onlyNetwork;
	private Boolean withCost;

	private int idError;
	private String mensajeError;
}
