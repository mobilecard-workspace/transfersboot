package com.addcel.transfersboot.dto.apto;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CostPaymentLocationNetwork implements Serializable {

	private static final long serialVersionUID = 8857449344337364822L;
	
	private String addressBranch;
	private String businessHours;
	private String customerfixfee;
	private String customerpercentageapplied;
	private String customerpercentagefee;
	private String deliveryTime;
	private String idCity;
	private String idCountry;
	private String idModePay;
	private String idState;
	private String latitude;
	private String logoName;
	private String longitude;
	private BigDecimal maxAmount;
	private BigDecimal minAmount;
	private String nameGroup;
	private String numberOfLocations;
	private BigDecimal outputAmount;
	private BigDecimal outputRate;
	private String payerNetworkLocation;
	private String phoneBranch;
	private Boolean routingRequired;
	private String viamericasfixfee;
	private String viamericaspercentagefee;
	private String idbranchpaymentlocation;

}
