package com.addcel.transfersboot.dto.apto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CostPaymentLocationNetworkResponse implements Serializable {

	private static final long serialVersionUID = -7619658819227052428L;

	private CostPaymentLocationNetwork[] locations;
	
	private int idError;
	private String mensajeError;
	

}
