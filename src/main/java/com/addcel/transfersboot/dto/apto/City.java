package com.addcel.transfersboot.dto.apto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class City implements Serializable {

	private static final long serialVersionUID = -9155618931607351030L;
	
	private String idCity;
    private String idState;
    private String idCountry;
    private String cityName;
    private String zipCode;

}
