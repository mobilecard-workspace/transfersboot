package com.addcel.transfersboot.dto.apto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CountrysResp implements Serializable {

	private static final long serialVersionUID = 9007176379298783579L;
	
	private SpGetCountries_Result[] countrys;
	private SpGetCountryStates_Result[] countryStates;
	private SpGetStateCities_Result[] city;

	private int idError;
	private String mensajeError;

}
