package com.addcel.transfersboot.dto.apto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SpGetPaymentModesbyCountry_Result implements Serializable {

	private static final long serialVersionUID = -4447553625640861780L;
	
	private String paymentModeId;
	private String paymentModeName;

}
