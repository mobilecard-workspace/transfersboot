package com.addcel.transfersboot.dto.apto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SpGetStateCities_Result implements Serializable {

	private static final long serialVersionUID = -4695845405328282239L;
	
	private String idCity;
    private String nameCity;

}
