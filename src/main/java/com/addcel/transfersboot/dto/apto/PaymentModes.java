package com.addcel.transfersboot.dto.apto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PaymentModes implements Serializable {
	
	private static final long serialVersionUID = 7932899163086167347L;

	private SpGetPaymentModesbyCountry_Result[] paymentModes;
	
	private int idError;
	private String mensajeError;

}
