package com.addcel.transfersboot.dto.apto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SpGetCountryStates_Result implements Serializable {
	
	private static final long serialVersionUID = 1189812871278698137L;
	
	private String idState;
    private String moneyTransferLicensed;
    private String nameState;

}
