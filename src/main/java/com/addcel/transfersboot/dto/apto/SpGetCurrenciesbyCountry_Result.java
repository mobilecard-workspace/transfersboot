package com.addcel.transfersboot.dto.apto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SpGetCurrenciesbyCountry_Result implements Serializable {

	private static final long serialVersionUID = -4080915870578410643L;
	
	private String ID;
    private String ISO_CURRENCY;
    private String NAME;

}
