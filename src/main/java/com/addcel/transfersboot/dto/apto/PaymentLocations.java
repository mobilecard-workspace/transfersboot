package com.addcel.transfersboot.dto.apto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PaymentLocations implements Serializable {

	private static final long serialVersionUID = -765921810494295976L;

	private SpGetReceiverPaymentLocations_Result[] locations;
	
	private int idError;
	private String mensajeError;

}
