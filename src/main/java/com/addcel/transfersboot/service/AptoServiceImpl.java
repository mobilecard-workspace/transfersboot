package com.addcel.transfersboot.service;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.addcel.transfersboot.dto.apto.CostPaymentLocationNetwork;
import com.addcel.transfersboot.dto.apto.CostPaymentLocationNetworkResponse;
import com.addcel.transfersboot.dto.apto.Countrys;
import com.addcel.transfersboot.dto.apto.CountrysResp;
import com.addcel.transfersboot.dto.apto.CurrencyByCountry;
import com.addcel.transfersboot.dto.apto.PaymentLocationNetwork;
import com.addcel.transfersboot.dto.apto.PaymentLocations;
import com.addcel.transfersboot.dto.apto.PaymentModes;
import com.addcel.transfersboot.dto.apto.SpGetCountries_Result;
import com.addcel.transfersboot.dto.apto.SpGetCountryStates_Result;
import com.addcel.transfersboot.dto.apto.SpGetCurrenciesbyCountry_Result;
import com.addcel.transfersboot.dto.apto.SpGetPaymentModesbyCountry_Result;
import com.addcel.transfersboot.dto.apto.SpGetReceiverPaymentLocations_Result;
import com.addcel.transfersboot.dto.apto.SpGetStateCities_Result;
import com.addcel.transfersboot.feign.apto.AptoClient;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Service
@Transactional
public class AptoServiceImpl implements AptoService {

	private final static Logger logger = Logger.getLogger(AptoServiceImpl.class);

	@Autowired
	private AptoClient aptoClient;

	@Override
	public ResponseEntity<String> getCountries(Integer idApp, String idioma, String json) {

		ResponseEntity<String> response = null;
		ResponseEntity<String> responseApto = null;
		CountrysResp resp = null;
		String output = "";

		try {

			logger.info("STARTING GET COUNTRYS FROM APTO - JSON - " + json);
			responseApto = aptoClient.getCountries();
			logger.info("APTO SERVICE RESPONSE :" + responseApto.getBody());

			if (responseApto.getStatusCode() == HttpStatus.OK) {

				JsonObject jsonObject = new JsonParser().parse(responseApto.getBody()).getAsJsonObject();
				JsonArray jsonArray = jsonObject.getAsJsonArray("countries");
				SpGetCountries_Result[] lista = new SpGetCountries_Result[jsonArray.size()];
				resp = new CountrysResp();

				for (int i = 0; i < jsonArray.size(); i++) {

					JsonObject country = (JsonObject) jsonArray.get(i).getAsJsonObject().get("country");
					SpGetCountries_Result result = new SpGetCountries_Result();
					result.setIdCountry(country.get("country_id").getAsString());
					result.setNameCountry(country.get("country_name").getAsString());
					lista[i] = result;

				}

				resp.setCountrys(lista);
				output = new Gson().toJson(resp);
				response = new ResponseEntity<String>(output, responseApto.getStatusCode());

			} else {
				resp = new CountrysResp();
				logger.error("ERROR GET COUNTRYS REQUEST - ERROR - " + responseApto.getStatusCode());
				resp.setIdError(-1);
				resp.setMensajeError("CANT GET INFORMATION");
				output = new Gson().toJson(resp);
				response = new ResponseEntity<String>(output, responseApto.getStatusCode());
			}

		} catch (ResponseStatusException e) {
			resp = new CountrysResp();
			logger.error("ERROR GET COUNTRYS - ERROR - " + e.getMessage());
			resp.setIdError(-1);
			resp.setMensajeError(e.getMessage());
			output = new Gson().toJson(resp);
			response = new ResponseEntity<String>(output, e.getStatus());
		} catch (Exception ex) {
			resp = new CountrysResp();
			logger.error("ERROR GET COUNTRYS REQUEST - ERROR - " + ex.getLocalizedMessage());
			ex.printStackTrace();
			resp.setIdError(-1);
			resp.setMensajeError(ex.getLocalizedMessage());
			output = new Gson().toJson(resp);
			response = new ResponseEntity<String>(output, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@Override
	public ResponseEntity<String> getCountrysStates(Integer idApp, String idioma, String json) {

		ResponseEntity<String> response = null;
		ResponseEntity<String> responseApto = null;
		CountrysResp resp = null;
		String output = "";

		try {

			Gson gson = new Gson();
			Countrys countrys = gson.fromJson(json, Countrys.class);

			logger.info("STARTING GET COUNTRYS STATES FROM APTO - JSON - " + json);
			responseApto = aptoClient.getCountrysStates(countrys.getIdCountry());
			logger.info("APTO SERVICE RESPONSE :" + responseApto.getBody());

			if (responseApto.getStatusCode() == HttpStatus.OK) {

				JsonObject jsonObject = new JsonParser().parse(responseApto.getBody()).getAsJsonObject();
				JsonArray jsonArray = jsonObject.getAsJsonArray("states");
				SpGetCountryStates_Result[] lista = new SpGetCountryStates_Result[jsonArray.size()];
				resp = new CountrysResp();

				for (int i = 0; i < jsonArray.size(); i++) {

					JsonObject state = (JsonObject) jsonArray.get(i).getAsJsonObject().get("state");

					SpGetCountryStates_Result result = new SpGetCountryStates_Result();
					result.setIdState(state.get("state_id").getAsString());
					result.setNameState(state.get("state_name").getAsString());
					lista[i] = result;

				}

				resp.setCountryStates(lista);
				output = new Gson().toJson(resp);
				response = new ResponseEntity<String>(output, responseApto.getStatusCode());

			} else {
				resp = new CountrysResp();
				logger.error("ERROR GET COUNTRYS STATES REQUEST - ERROR - " + responseApto.getStatusCode());
				resp.setIdError(-1);
				resp.setMensajeError("CANT GET INFORMATION");
				output = new Gson().toJson(resp);
				response = new ResponseEntity<String>(output, responseApto.getStatusCode());
			}

		} catch (ResponseStatusException e) {
			resp = new CountrysResp();
			logger.error("ERROR GET COUNTRYS STATES - ERROR - " + e.getMessage());
			resp.setIdError(-1);
			resp.setMensajeError(e.getMessage());
			output = new Gson().toJson(resp);
			response = new ResponseEntity<String>(output, e.getStatus());
		} catch (Exception ex) {
			resp = new CountrysResp();
			logger.error("ERROR GET COUNTRYS STATES REQUEST - ERROR - " + ex.getLocalizedMessage());
			ex.printStackTrace();
			resp.setIdError(-1);
			resp.setMensajeError(ex.getLocalizedMessage());
			output = new Gson().toJson(resp);
			response = new ResponseEntity<String>(output, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@Override
	public ResponseEntity<String> getStateCities(Integer idApp, String idioma, String json) {

		ResponseEntity<String> response = null;
		ResponseEntity<String> responseApto = null;
		CountrysResp resp = null;
		String output = "";

		try {

			Gson gson = new Gson();
			Countrys countrys = gson.fromJson(json, Countrys.class);

			logger.info("STARTING GET STATES CITIES FROM APTO - JSON - " + json);
			responseApto = aptoClient.getStateCities(countrys.getIdCountry());
			logger.info("APTO SERVICE RESPONSE :" + responseApto.getBody());

			if (responseApto.getStatusCode() == HttpStatus.OK) {

				JsonObject jsonObject = new JsonParser().parse(responseApto.getBody()).getAsJsonObject();
				JsonArray jsonArray = jsonObject.getAsJsonArray("cities");
				SpGetStateCities_Result[] lista = new SpGetStateCities_Result[jsonArray.size()];
				resp = new CountrysResp();

				for (int i = 0; i < jsonArray.size(); i++) {

					JsonObject city = (JsonObject) jsonArray.get(i).getAsJsonObject().get("city");
					SpGetStateCities_Result result = new SpGetStateCities_Result();
					result.setIdCity(city.get("city_id").getAsString());
					result.setNameCity(city.get("city_name").getAsString());
					lista[i] = result;

				}

				resp.setCity(lista);
				output = new Gson().toJson(resp);
				response = new ResponseEntity<String>(output, responseApto.getStatusCode());

			} else {
				resp = new CountrysResp();
				logger.error("ERROR GET STATES CITIES REQUEST - ERROR - " + responseApto.getStatusCode());
				resp.setIdError(-1);
				resp.setMensajeError("CANT GET INFORMATION");
				output = new Gson().toJson(resp);
				response = new ResponseEntity<String>(output, responseApto.getStatusCode());
			}

		} catch (ResponseStatusException e) {
			resp = new CountrysResp();
			logger.error("ERROR GET STATES CITIES - ERROR - " + e.getMessage());
			resp.setIdError(-1);
			resp.setMensajeError(e.getMessage());
			output = new Gson().toJson(resp);
			response = new ResponseEntity<String>(output, e.getStatus());
		} catch (Exception ex) {
			resp = new CountrysResp();
			logger.error("ERROR GET STATES CITIES REQUEST - ERROR - " + ex.getLocalizedMessage());
			ex.printStackTrace();
			resp.setIdError(-1);
			resp.setMensajeError(ex.getLocalizedMessage());
			output = new Gson().toJson(resp);
			response = new ResponseEntity<String>(output, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@Override
	public ResponseEntity<String> getPaymentModes(Integer idApp, String idioma, String json) {

		ResponseEntity<String> response = null;
		ResponseEntity<String> responseApto = null;
		PaymentModes resp = null;
		String output = "";

		try {

			Gson gson = new Gson();
			Countrys countrys = gson.fromJson(json, Countrys.class);

			logger.info("STARTING GET PAYMENT MODES FROM APTO - JSON - " + json);
			responseApto = aptoClient.getPaymentModes(countrys.getIdCountry());
			logger.info("APTO SERVICE RESPONSE :" + responseApto.getBody());

			if (responseApto.getStatusCode() == HttpStatus.OK) {

				JsonObject jsonObject = new JsonParser().parse(responseApto.getBody()).getAsJsonObject();
				JsonArray jsonArray = jsonObject.getAsJsonArray("payment_modes");
				SpGetPaymentModesbyCountry_Result[] lista = new SpGetPaymentModesbyCountry_Result[jsonArray.size()];
				resp = new PaymentModes();

				for (int i = 0; i < jsonArray.size(); i++) {

					JsonObject payment_mode = (JsonObject) jsonArray.get(i).getAsJsonObject().get("payment_mode");

					SpGetPaymentModesbyCountry_Result result = new SpGetPaymentModesbyCountry_Result();
					result.setPaymentModeId(payment_mode.get("payment_mode").getAsString());
					result.setPaymentModeName(payment_mode.get("payment_mode_name").getAsString());
					lista[i] = result;

				}

				resp.setPaymentModes(lista);
				output = new Gson().toJson(resp);
				response = new ResponseEntity<String>(output, responseApto.getStatusCode());

			} else {
				resp = new PaymentModes();
				logger.error("ERROR GET PAYMENT MODES REQUEST - ERROR - " + responseApto.getStatusCode());
				resp.setIdError(-1);
				resp.setMensajeError("CANT GET INFORMATION");
				output = new Gson().toJson(resp);
				response = new ResponseEntity<String>(output, responseApto.getStatusCode());
			}

		} catch (ResponseStatusException e) {
			resp = new PaymentModes();
			logger.error("ERROR GET PAYMENT MODES - ERROR - " + e.getMessage());
			resp.setIdError(-1);
			resp.setMensajeError(e.getMessage());
			output = new Gson().toJson(resp);
			response = new ResponseEntity<String>(output, e.getStatus());
		} catch (Exception ex) {
			resp = new PaymentModes();
			logger.error("ERROR GET PAYMENT MODES REQUEST - ERROR - " + ex.getLocalizedMessage());
			ex.printStackTrace();
			resp.setIdError(-1);
			resp.setMensajeError(ex.getLocalizedMessage());
			output = new Gson().toJson(resp);
			response = new ResponseEntity<String>(output, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@Override
	public ResponseEntity<String> getPaymentCurrenciesbyCountry(Integer idApp, String idioma, String json) {

		ResponseEntity<String> response = null;
		ResponseEntity<String> responseApto = null;
		CurrencyByCountry resp = null;
		String output = "";

		try {
			Gson gson = new Gson();
			Countrys countrys = gson.fromJson(json, Countrys.class);

			logger.info("STARTING GET CURRENCY BY COUNTRY FROM APTO - JSON - " + json);
			responseApto = aptoClient.getPaymentCurrenciesbyCountry(countrys.getIdCountry());
			logger.info("APTO SERVICE RESPONSE :" + responseApto.getBody());

			if (responseApto.getStatusCode() == HttpStatus.OK) {

				JsonObject jsonObject = new JsonParser().parse(responseApto.getBody()).getAsJsonObject();
				JsonArray jsonArray = jsonObject.getAsJsonArray("payment_currencies");
				SpGetCurrenciesbyCountry_Result[] lista = new SpGetCurrenciesbyCountry_Result[jsonArray.size()];
				resp = new CurrencyByCountry();

				for (int i = 0; i < jsonArray.size(); i++) {

					JsonObject payment = (JsonObject) jsonArray.get(i).getAsJsonObject().get("payment_currency");

					SpGetCurrenciesbyCountry_Result result = new SpGetCurrenciesbyCountry_Result();
					result.setID(payment.get("currency_id").getAsString());
					result.setNAME(payment.get("currency_name").getAsString());
					lista[i] = result;

				}

				resp.setCurrency(lista);
				output = new Gson().toJson(resp);
				response = new ResponseEntity<String>(output, responseApto.getStatusCode());

			} else {
				resp = new CurrencyByCountry();
				logger.error("ERROR GET CURRENCY BY COUNTRY REQUEST - ERROR - " + responseApto.getStatusCode());
				resp.setIdError(-1);
				resp.setMensajeError("CANT GET INFORMATION");
				output = new Gson().toJson(resp);
				response = new ResponseEntity<String>(output, responseApto.getStatusCode());
			}

		} catch (ResponseStatusException e) {
			resp = new CurrencyByCountry();
			logger.error("ERROR GET CURRENCY BY COUNTRY - ERROR - " + e.getMessage());
			resp.setIdError(-1);
			resp.setMensajeError(e.getMessage());
			output = new Gson().toJson(resp);
			response = new ResponseEntity<String>(output, e.getStatus());
		} catch (Exception ex) {
			resp = new CurrencyByCountry();
			logger.error("ERROR GET CURRENCY BY COUNTRY REQUEST - ERROR - " + ex.getLocalizedMessage());
			ex.printStackTrace();
			resp.setIdError(-1);
			resp.setMensajeError(ex.getLocalizedMessage());
			output = new Gson().toJson(resp);
			response = new ResponseEntity<String>(output, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@Override
	public ResponseEntity<String> getPaymentLocations(Integer idApp, String idioma, String json) {

		ResponseEntity<String> response = null;
		ResponseEntity<String> responseApto = null;
		PaymentLocations resp = null;
		String output = "";

		try {

			Gson gson = new Gson();
			Countrys countrys = gson.fromJson(json, Countrys.class);

			logger.info("STARTING GET PAYMENT LOCATIONS FROM APTO - JSON - " + json);
			responseApto = aptoClient.getPaymentLocations(countrys.getIdCountry(), countrys.getIdCity(),
					countrys.getPayMode());
			logger.info("APTO SERVICE RESPONSE :" + responseApto.getBody());

			if (responseApto.getStatusCode() == HttpStatus.OK) {

				JsonObject jsonObject = new JsonParser().parse(responseApto.getBody()).getAsJsonObject();
				JsonArray jsonArray = jsonObject.getAsJsonArray("payment_locations");
				SpGetReceiverPaymentLocations_Result[] lista = new SpGetReceiverPaymentLocations_Result[jsonArray
						.size()];
				resp = new PaymentLocations();

				for (int i = 0; i < jsonArray.size(); i++) {

					JsonObject paymentLocations = (JsonObject) jsonArray.get(i).getAsJsonObject()
							.get("payment_location");

					SpGetReceiverPaymentLocations_Result result = new SpGetReceiverPaymentLocations_Result();
					result.setIdPaymentLocation(paymentLocations.get("payment_location_id").getAsString());
					result.setNamePayer(paymentLocations.get("payment_location_name").getAsString());
					result.setNamePaymentLocation(paymentLocations.get("payment_network_name").getAsString());
					result.setAddressPaymentLocation(paymentLocations.get("address").getAsString());
					result.setBusinessHours(paymentLocations.get("business_hours").getAsString());
					result.setLatitude(paymentLocations.get("latitude").getAsString());
					result.setLongitude(paymentLocations.get("longitude").getAsString());

					lista[i] = result;

				}

				resp.setLocations(lista);
				output = new Gson().toJson(resp);
				response = new ResponseEntity<String>(output, responseApto.getStatusCode());

			} else {
				resp = new PaymentLocations();
				logger.error("ERROR GET COUNTRYS STATES REQUEST - ERROR - " + responseApto.getStatusCode());
				resp.setIdError(-1);
				resp.setMensajeError("CANT GET INFORMATION");
				output = new Gson().toJson(resp);
				response = new ResponseEntity<String>(output, responseApto.getStatusCode());
			}

		} catch (ResponseStatusException e) {
			resp = new PaymentLocations();
			logger.error("ERROR GET PAYMENT LOCATIONS - ERROR - " + e.getMessage());
			resp.setIdError(-1);
			resp.setMensajeError(e.getMessage());
			output = new Gson().toJson(resp);
			response = new ResponseEntity<String>(output, e.getStatus());
		} catch (Exception ex) {
			resp = new PaymentLocations();
			logger.error("ERROR GET PAYMENT LOCATIONS REQUEST - ERROR - " + ex.getLocalizedMessage());
			ex.printStackTrace();
			resp.setIdError(-1);
			resp.setMensajeError(ex.getLocalizedMessage());
			output = new Gson().toJson(resp);
			response = new ResponseEntity<String>(output, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@Override
	public ResponseEntity<String> getCostPaymentLocationNetwork(Integer idApp, String idioma, String json) {

		ResponseEntity<String> response = null;
		ResponseEntity<String> responseApto = null;
		CostPaymentLocationNetworkResponse resp = null;
		String output = "";

		try {

			Gson gson = new Gson();
			PaymentLocationNetwork location = gson.fromJson(json, PaymentLocationNetwork.class);

			logger.info("STARTING GET COST PAYMENT LOCATION NETWORK FROM APTO - JSON - " + json);
			responseApto = aptoClient.getCostPaymentLocationNetwork(location.getIdCountry(), location.getIdPaymentMode());
			logger.info("APTO SERVICE RESPONSE :" + responseApto.getBody());
			
			if (responseApto.getStatusCode() == HttpStatus.OK) {

				JsonObject jsonObject = new JsonParser().parse(responseApto.getBody()).getAsJsonObject();
				JsonArray jsonArray = jsonObject.getAsJsonArray("payment_networks");
				CostPaymentLocationNetwork[] lista = new CostPaymentLocationNetwork[jsonArray.size()];
				resp = new CostPaymentLocationNetworkResponse();

				for (int i = 0; i < jsonArray.size(); i++) {

					JsonObject paymentNetwork = (JsonObject) jsonArray.get(i).getAsJsonObject().get("payment_network");
					CostPaymentLocationNetwork result = new CostPaymentLocationNetwork();
					result.setIdbranchpaymentlocation(paymentNetwork.get("payment_network_id").getAsString());
					result.setNameGroup(paymentNetwork.get("payment_network_name").getAsString());
					result.setLogoName(paymentNetwork.get("logo_name").getAsString());
					result.setNumberOfLocations(paymentNetwork.get("number_of_locations").getAsString());
					result.setPhoneBranch(paymentNetwork.get("recipient_phone_required").getAsString());
					result.setRoutingRequired(Boolean.parseBoolean(paymentNetwork.get("routing_required").getAsString()));
					lista[i] = result;

				}

				resp.setLocations(lista);
				output = new Gson().toJson(resp);
				response = new ResponseEntity<String>(output, responseApto.getStatusCode());

			} else {
				resp = new CostPaymentLocationNetworkResponse();
				logger.error("ERROR GET COST PAYMENT LOCATION NETWORK REQUEST - ERROR - " + responseApto.getStatusCode());
				resp.setIdError(-1);
				resp.setMensajeError("CANT GET INFORMATION");
				output = new Gson().toJson(resp);
				response = new ResponseEntity<String>(output, responseApto.getStatusCode());
			}
			
		} catch (ResponseStatusException e) {
			resp = new CostPaymentLocationNetworkResponse();
			logger.error("ERROR GET COST PAYMENT LOCATION NETWORK - ERROR - " + e.getMessage());
			resp.setIdError(-1);
			resp.setMensajeError(e.getMessage());
			output = new Gson().toJson(resp);
			response = new ResponseEntity<String>(output, e.getStatus());
		} catch (Exception ex) {
			resp = new CostPaymentLocationNetworkResponse();
			logger.error("ERROR GET COST PAYMENT LOCATION NETWORK REQUEST - ERROR - " + ex.getLocalizedMessage());
			ex.printStackTrace();
			resp.setIdError(-1);
			resp.setMensajeError(ex.getLocalizedMessage());
			output = new Gson().toJson(resp);
			response = new ResponseEntity<String>(output, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

}
