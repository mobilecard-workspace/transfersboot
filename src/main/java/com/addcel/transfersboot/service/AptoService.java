package com.addcel.transfersboot.service;

import org.springframework.http.ResponseEntity;

public interface AptoService {
	
	public ResponseEntity<String> getCountries(Integer idApp, String idioma, String json);
	public ResponseEntity<String> getCountrysStates(Integer idApp, String idioma, String json);
	public ResponseEntity<String> getStateCities(Integer idApp, String idioma, String json);
	public ResponseEntity<String> getPaymentModes(Integer idApp, String idioma, String json);
	public ResponseEntity<String> getPaymentCurrenciesbyCountry(Integer idApp, String idioma, String json);
	public ResponseEntity<String> getPaymentLocations(Integer idApp, String idioma, String json);
	public ResponseEntity<String> getCostPaymentLocationNetwork(Integer idApp, String idioma, String json);

}
