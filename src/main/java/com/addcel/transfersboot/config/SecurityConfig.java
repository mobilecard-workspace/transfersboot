package com.addcel.transfersboot.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.addcel.transfersboot.dto.security.AppUser;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private AuthenticationEntryPoint authEntryPoint;

	@Autowired
	private AppUser authUser;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http .cors()
    	.and().csrf().disable()
		.authorizeRequests()
		.anyRequest().authenticated()
		.and()
		.httpBasic()
		.authenticationEntryPoint(authEntryPoint);

	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser(authUser.getUsername()).password("{noop}" + authUser.getPassword()).roles("USER");
	}

}
