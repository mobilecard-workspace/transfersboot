package com.addcel.transfersboot.data.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.addcel.transfersboot.data.entity.AppAuthorization;

@Repository
public interface AppAuthorizationDao extends JpaRepository<AppAuthorization, Integer> {
	
	public Optional<AppAuthorization> findByIdApplicationAndActivo(String idApplication, String activo);

}
