package com.addcel.transfersboot.data.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "APP_AUTHORIZATION")
@Setter
@Getter
public class AppAuthorization implements Serializable {

	private static final long serialVersionUID = -8366645120828459029L;

	@Id
    @Column(name = "id_app_auth")
    private Long idAppAuth;
	
    @Column(name = "id_application")
    private String idApplication;
    
    @Column(name = "username")
    private String username;
    
    @Column(name = "password")
    private String password;
    
    @Column(name = "activo")
    private String activo;

}
