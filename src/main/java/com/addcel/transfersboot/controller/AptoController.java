package com.addcel.transfersboot.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.transfersboot.service.AptoService;

@RestController
@RequestMapping
public class AptoController {

	private final static Logger logger = Logger.getLogger(AptoController.class);

	@Autowired
	private AptoService aptoService;

	@GetMapping("/{idApp}/{idioma}/get/countrys")
	public ResponseEntity<String> getCountries(@PathVariable Integer idApp, @PathVariable String idioma,
			@RequestParam("json") String json) {
		ResponseEntity<String> responseEntity = null;
		try {
			responseEntity = aptoService.getCountries(idApp, idioma, json);
		} catch (Exception e) {
			logger.error("Metodo getCountries: ", e);
			responseEntity = new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}

	@GetMapping("/{idApp}/{idioma}/get/countrysStates")
	public ResponseEntity<String> getCountrysStates(@PathVariable Integer idApp, @PathVariable String idioma,
			@RequestParam("json") String json) {
		ResponseEntity<String> responseEntity = null;
		try {
			responseEntity = aptoService.getCountrysStates(idApp, idioma, json);
		} catch (Exception e) {
			logger.error("Metodo getCountrysStates: ", e);
			responseEntity = new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}

	@GetMapping("/{idApp}/{idioma}/get/statesCitys")
	public ResponseEntity<String> getStateCities(@PathVariable Integer idApp, @PathVariable String idioma,
			@RequestParam("json") String json) {
		ResponseEntity<String> responseEntity = null;
		try {
			responseEntity = aptoService.getStateCities(idApp, idioma, json);
		} catch (Exception e) {
			logger.error("Metodo getStateCities: ", e);
			responseEntity = new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}

	@GetMapping("/{idApp}/{idioma}/get/payment/modes")
	public ResponseEntity<String> getPaymentModes(@PathVariable Integer idApp, @PathVariable String idioma,
			@RequestParam("json") String json) {
		ResponseEntity<String> responseEntity = null;
		try {
			responseEntity = aptoService.getPaymentModes(idApp, idioma, json);
		} catch (Exception e) {
			logger.error("Metodo getPaymentModes: ", e);
			responseEntity = new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@GetMapping("/{idApp}/{idioma}/get/currency/country")
	public ResponseEntity<String> getPaymentCurrenciesbyCountry(@PathVariable Integer idApp, @PathVariable String idioma,
			@RequestParam("json") String json) {
		ResponseEntity<String> responseEntity = null;
		try {
			responseEntity = aptoService.getPaymentCurrenciesbyCountry(idApp, idioma, json);
		} catch (Exception e) {
			logger.error("Metodo getPaymentCurrenciesbyCountry: ", e);
			responseEntity = new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@GetMapping("/{idApp}/{idioma}/get/payment/locations")
	public ResponseEntity<String> getPaymentLocations(@PathVariable Integer idApp, @PathVariable String idioma,
			@RequestParam("json") String json) {
		ResponseEntity<String> responseEntity = null;
		try {
			responseEntity = aptoService.getPaymentLocations(idApp, idioma, json);
		} catch (Exception e) {
			logger.error("Metodo getPaymentLocations: ", e);
			responseEntity = new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@GetMapping("/{idApp}/{idioma}/get/costPaymentLocationNetwork")
	public ResponseEntity<String> getCostPaymentLocationNetwork(@PathVariable Integer idApp, @PathVariable String idioma,
			@RequestParam("json") String json) {
		ResponseEntity<String> responseEntity = null;
		try {
			responseEntity = aptoService.getCostPaymentLocationNetwork(idApp, idioma, json);
		} catch (Exception e) {
			logger.error("Metodo getCostPaymentLocationNetwork: ", e);
			responseEntity = new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}

}
