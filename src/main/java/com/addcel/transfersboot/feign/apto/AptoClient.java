package com.addcel.transfersboot.feign.apto;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "aptoClient", url = "${apto.url.base}", configuration = FeignClientAptoConfiguration.class)
public interface AptoClient {
	
	@GetMapping(value = "/remittances/catalogs/countries")
	ResponseEntity<String> getCountries();

	@GetMapping(value = "/remittances/catalogs/countries/{idCountry}/states")
	ResponseEntity<String> getCountrysStates(@PathVariable("idCountry") String idCountry);
	
	@GetMapping(value = "/remittances/catalogs/countries/{idCountry}/cities")
	ResponseEntity<String> getStateCities(@PathVariable("idCountry") String idCountry);
	
	@GetMapping(value = "/remittances/catalogs/countries/{idCountry}/payment-modes")
	ResponseEntity<String> getPaymentModes(@PathVariable("idCountry") String idCountry);
	
	@GetMapping(value = "/remittances/catalogs/countries/{idCountry}/payment-currencies")
	ResponseEntity<String> getPaymentCurrenciesbyCountry(@PathVariable("idCountry") String idCountry);
	
	@GetMapping(value = "/remittances/catalogs/countries/{idCountry}/payment-locations")
	ResponseEntity<String> getPaymentLocations(@PathVariable("idCountry") String idCountry, @RequestParam("city_id") String city_id, @RequestParam("payment_mode") String payment_mode);
	
	@GetMapping(value = "/remittances/catalogs/countries/{idCountry}/payment-modes/{paymentMode}/payment-networks")
	ResponseEntity<String> getCostPaymentLocationNetwork(@PathVariable("idCountry") String idCountry, @PathVariable("paymentMode") String paymentMode);
	
//	@PostMapping(value = "${forte.customers.save.simple.endpoint}{idOrganization}/locations/loc_{idLocation}/customers/")
//	@Headers("Content-Type: application/json")
//	ResponseEntity<CustomerSimpleResponse> customerAddSimple(@PathVariable("idOrganization") String idOrganization,
//			@PathVariable("idLocation") String idLocation, CustomerSimpleRequest customer);
}
