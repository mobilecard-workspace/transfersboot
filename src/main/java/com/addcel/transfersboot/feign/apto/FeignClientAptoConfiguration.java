package com.addcel.transfersboot.feign.apto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.Logger;
import feign.auth.BasicAuthRequestInterceptor;
import feign.slf4j.Slf4jLogger;

@Configuration
public class FeignClientAptoConfiguration {
	
	@Value("${apto.service.user}")
	private String aptoUser;
	
	@Value("${apto.service.pass}")
	private String aptoPass;
	
	@Bean
	Logger feignLoggerLevel(){
		return new Slf4jLogger();
	}
	
	@Bean
	public BasicAuthRequestInterceptor basicAuthRequestInterceptor() {
		return new BasicAuthRequestInterceptor(aptoUser, aptoPass);
	}

}
