package com.addcel.transfersboot;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.addcel.transfersboot.data.dao.AppAuthorizationDao;
import com.addcel.transfersboot.data.entity.AppAuthorization;
import com.addcel.transfersboot.dto.security.AppUser;

@SpringBootApplication
@ComponentScan(basePackages = "com.addcel.transfersboot")
@EnableFeignClients
public class TransfersBootApplication extends SpringBootServletInitializer {
	
	private final static Logger logger = Logger.getLogger(TransfersBootApplication.class);

	@Value("${app.auth.id}")
	private String appId;

	@Value("${app.auth.status}")
	private String appStatus;

	@Autowired
	private AppAuthorizationDao appAuthorizationDao;
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(TransfersBootApplication.class);
	}

	@Bean
	@PostConstruct
	public AppUser appUser() {
		AppUser appUser = new AppUser();
		try {
			Optional<AppAuthorization> auth = appAuthorizationDao.findByIdApplicationAndActivo(appId, appStatus);
			if (auth != null & auth.isPresent()) {
				appUser.setUsername(auth.get().getUsername());
				appUser.setPassword(auth.get().getPassword());
			} else {
				logger.info("No es posible obtener los datos de autenticacion para Mobile Card");
			}
		} catch (Exception e) {
			logger.info("Error al obtener la autenticacion desde BD " + e.getMessage());
		}
		return appUser;
	}

	public static void main(String[] args) {
		SpringApplication.run(TransfersBootApplication.class, args);
	}

}
